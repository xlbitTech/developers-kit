  <HotelDescriptiveContent HotelCode="123" HotelName="Frangart Inn">
    <FacilityInfo>
      <GuestRooms>

        <!-- "single", formerly known as "EZ" -->

        <GuestRoom Code="single" MaxOccupancy="2" MinOccupancy="1" ID="EZ">
          <TypeRoom StandardOccupancy="2" RoomClassificationCode="42"/>
        </GuestRoom>

        <!-- "double", formerly known as "DZ" -->

        <GuestRoom Code="double" MaxOccupancy="2" MinOccupancy="1" ID="DZ">
          <TypeRoom StandardOccupancy="2" RoomClassificationCode="42"/>
        </GuestRoom>

       </GuestRooms>
      </FacilityInfo>
    </HotelDescriptiveContent>
