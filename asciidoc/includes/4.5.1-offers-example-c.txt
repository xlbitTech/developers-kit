<Offers>
  <Offer>
    <OfferRules>
      <OfferRule>
        <LengthsOfStay>
          <LengthOfStay Time="4" TimeUnit="Day" MinMaxMessageType="SetMinLOS" />
          <LengthOfStay Time="4" TimeUnit="Day" MinMaxMessageType="SetMaxLOS" />
        </LengthsOfStay>
        <DOW_Restrictions>
          <ArrivalDaysOfWeek   Sun="1" Mon="0" Tue="0" Weds="0" Thur="0" Fri="0" Sat="0" />
          <DepartureDaysOfWeek Sun="0" Mon="0" Tue="0" Weds="0" Thur="1" Fri="0" Sat="0" />
        </DOW_Restrictions>
        <Occupancy AgeQualifyingCode="10" MinAge="16" />
        <Occupancy AgeQualifyingCode="8" />
      </OfferRule>
    </OfferRules>
  </Offer>
  <Offer>
    <Discount Percent="100" NightsRequired="4" NightsDiscounted="1" />
  </Offer>
</Offers>
