<RoomStays>

    <RoomStay>

        <RoomTypes>
            <RoomType RoomTypeCode="bigsuite" RoomClassificationCode="42"/>
     </RoomTypes>

        <RatePlans>
            <RatePlan RatePlanCode="123456-xyz">
                <Commission Percent="15"/>
                <!-- Code 1 -> All inclusive -->
                <MealsIncluded MealPlanIndicator="true" MealPlanCodes="1"/>
            </RatePlan>
        </RatePlans>

        <!-- 2 adults + 1 child + 1 child = 4 guests -->
        <GuestCounts>
            <!-- 2 adults -->
            <GuestCount Count="2"/>
            <!-- 1 child -->
            <GuestCount Count="1" Age="9"/>
            <!-- 1 child -->
            <GuestCount Count="1" Age="3"/>
        </GuestCounts>

        <TimeSpan Start="2012-01-01" End="2012-01-12"/>

        <Guarantee>
            <GuaranteesAccepted>
                <GuaranteeAccepted>
                    <PaymentCard CardCode="VI"
                                 ExpireDate="1216">
                        <CardHolderName>Otto Mustermann</CardHolderName>
                        <CardNumber>
                            <PlainText>4444333322221111</PlainText>
                        </CardNumber>
                    </PaymentCard>
                </GuaranteeAccepted>
            </GuaranteesAccepted>
        </Guarantee>

        <Total AmountAfterTax="299" CurrencyCode="EUR"/>

    </RoomStay>

</RoomStays>
