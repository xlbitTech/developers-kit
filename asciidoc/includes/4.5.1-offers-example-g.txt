<Offers>
    <Offer>
        <OfferRules>
            <OfferRule>
                <LengthsOfStay>
                    <LengthOfStay Time="4" TimeUnit="Day" MinMaxMessageType="SetMinLOS" />
                </LengthsOfStay>
                <Occupancy AgeQualifyingCode="10" MinAge="16" />
                <Occupancy AgeQualifyingCode="8" />
                </OfferRule>
            </OfferRules>
        </Offer>
    <Offer>
        <Discount Percent="100" NightsRequired="4"
                  NightsDiscounted="1" DiscountPattern="0001" />
    </Offer>
    <Offer>
        <Discount Percent="100" />
        <Guests>
            <Guest AgeQualifyingCode="8" MaxAge="5" MinCount="1"
                   FirstQualifyingPosition="1" LastQualifyingPosition="1" />
        </Guests>
    </Offer>
</Offers>
