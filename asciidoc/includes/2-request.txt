POST / HTTP/1.1
Authorization: Basic Y2hyaXM6c2VjcmV0
Host: localhost
Accept: */*
X-AlpineBits-ClientProtocolVersion: 2015-07
X-AlpineBits-ClientID: sample-client.php v. 2015-07 1.0
Content-Length: 1989
Expect: 100-continue
Content-Type: multipart/form-data; boundary=----------------------------9d7042ecb251

------------------------------9d7042ecb251
Content-Disposition: form-data; name="action"

OTA_HotelAvailNotif:FreeRooms
------------------------------9d7042ecb251
Content-Disposition: form-data; name="request"

<?xml version="1.0" encoding="UTF-8"?>

<OTA_HotelAvailNotifRQ
         [...]
</OTA_HotelAvailNotifRQ>
------------------------------9d7042ecb251--
