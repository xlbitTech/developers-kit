<OTA_HotelDescriptiveContentNotifRQ 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns="http://www.opentravel.org/OTA/2003/05" 
  xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05
                      OTA_HotelDescriptiveContentNotifRQ.xsd" 
  Version="8.000">

<HotelDescriptiveContents>
  <HotelDescriptiveContent HotelCode="123" HotelName="Frangart Inn">
    <FacilityInfo>
      <GuestRooms>

        <!-- the heading GuestRoom element is used to define a category 
             and its basic description -->

        <GuestRoom Code="DZ" MaxOccupancy="2" MinOccupancy="1" MaxChildOccupancy="1">
          <!-- ... see below ... -->
        </GuestRoom>

        <!-- the follow-up GuestRoom elements list the specific
             rooms in the category -->

        <GuestRoom Code="DZ">
          <TypeRoom RoomID="101"/>
        </GuestRoom>

        <GuestRoom Code="DZ">
          <TypeRoom RoomID="102"/>
        </GuestRoom>

       </GuestRooms>
      </FacilityInfo>
    </HotelDescriptiveContent>
  </HotelDescriptiveContents>

</OTA_HotelDescriptiveContentNotifRQ>
