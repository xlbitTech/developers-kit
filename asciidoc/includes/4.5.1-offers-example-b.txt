<Offers>
    <Offer>
        <OfferRules>
            <OfferRule MaxAdvancedBookingOffset="P7D">
                <Occupancy AgeQualifyingCode="10" MinAge="16" />
                <Occupancy AgeQualifyingCode="8" />
            </OfferRule>
        </OfferRules>
    </Offer>
</Offers>
