[[anchor-C]]
== C. Protocol Version Compatibility


=== C.1. Major overhaul in version 2018-10

==== Housekeeping / Handshaking

The ``Housekeeping`` action has been renamed to ``Handshaking`` and the relative chapter has been completely rewritten introducing *breaking changes*.
Through the introduction of the ``OTA_Ping:Handshaking`` action it is now possible for clients to expose the capabilities they support.


==== New action GuestRequests (Push)

This functionality was introduced in version 2018-10.

It is now possible to push ``GuestRequests`` instead of polling them.
Please note that this functionality relies on a few arrangements between the two parties that are not negotiated through {AlpineBitsR}, namely the endpoint (URL) where the client can receive the pushed ``GuestRequests``, its credentials, etc.

==== New attribute RoomType

This functionality was introduced in version 2018-10.

For ``Inventory`` and ``GuestRequests``, in addition to the [xml_attribute_name]#RoomClassificationCode# attribute, the optional attribute [xml_attribute_name]#RoomType# has been added in order to allow a more precise classification of the guest accommodation.

==== Reference to the online presence of the lodging structure

This functionality was introduced in version 2018-10.

For ``Inventory``, the element [xml_element_name]#ContactInfos# can be used to transmit URIs where the lodging structure has a presence (like social networks, review aggregators, etc.).

==== Review price calculation

These changes were performed to the price calculation algorithm:

- the [xml_element_name]#AdditionalGuestAmount# element with attribute [xml_attribute_name]#AgeQualifyingCode# equal to 10 becomes optional;
- the [xml_element_name]#AdditionalGuestAmounts# element can be transmitted also if the [xml_attribute_name]#MaxOccupancy# attribute is equal to the [xml_attribute_name]#StandardOccupancy#;

==== Interaction with channel managers

- For ``BaseRates``, a new case is described that allows servers to exchange information with clients that are only able to send RatePlans overlay information.
- For ``GuestRequests`` of type ``Reservation``, a recommendation to set the value of the mandatory [xml_attribute_name]#RatePlanCode# attribute to the string "Unknown" has been added to ease the integration with channel managers that do not provide a reference to the {AlpineBitsR} ``RatePlan`` which the ``GuestRequest`` actually refers to.

==== Server request for complete data set

In case of suspects of mis-alignment of data, servers can now request the client to send a full data set.


=== C.2. Major overhaul in version 2017-10


==== pass:normal[{AlpineBitsR} server response outcomes]

The {AlpineBitsR} response outcomes (*success*, *advisory*, *warning*, *error*) are now uniformly and more thoroughly defined in the new <<anchor-A,appendix A>>.

In particular the distinction between warning and error outcomes is more in line with the OTA documentation <<anchor-footnote3,^3^>> (see the section "OpenTravel Message Success, Warnings &amp; Errors Elements").

Implementations of 2015-07b will likely signal as errors things that in 2017-10 would be considered warnings.
This should not lead to breakage, however, since both (warnings and errors) unambiguously indicate failed requests and this has been spelled out with greater clarity in the 2017-10 document.

In any case implementors are invited to have a close look at the new <<anchor-A,appendix A>>.

==== FreeRooms

Two new features have been added to FreeRooms: transmission of the number of bookable rooms and purge requests.


==== GuestRequests

Two new features have been added to GuestRequests: commissions and encrypted credit card numbers.
The credit card holder name was made optional.
Some info on how to fill out the ReservationID was added to the best practice section.


==== SimplePackages

The feature has been removed in version 2017-10.


==== Inventory

Inventory messages were introduced in version 2014-04 and overhauled in version 2015-07.

In version 2017-10 the section was again changed significantly.
They former Inventory request action has been heavily refactored and split into four different actions:

1. *Inventory/Basic: (push)* action `OTA_HotelDescriptiveContentNotif:Inventory` (same string was also used in previous version) is now only used to send room category information and room lists (unlike previous versions, where it had multiple uses),

2. *Inventory/Basic (pull)*: action `OTA_HotelDescriptiveInfo:Inventory` (new) is used to request basic information.

3. *Inventory/HotelInfo (push)*: action `OTA_HotelDescriptiveContentNotif:Info` (new) is used to sends additional descriptive content and

4. *Inventory/HotelInfo (pull)*: action `OTA_HotelDescriptiveInfo:Info` (new) is used to request additional descriptive content.

Of course the corresponding action capabilities have been defined.

Due to the split, these capabilities are no longer necessary and were *removed*:

* `OTA_HotelDescriptiveContentNotif_Inventory_accept_basic`

* `OTA_HotelDescriptiveContentNotif_Inventory_accept_additional`


==== RatePlans

Concerning *booking rules*, previously the attribute [xml_attribute_name]#MinMaxMessageType# of [xml_element_name]#LengthOfStay# could assume the *two* values `SetMinLOS` and `SetMaxLOS` and there were two corresponding capabilities (`OTA_HotelRatePlanNotif_accept_MinLOS` and `OTA_HotelRatePlanNotif_accept_MaxLOS`).
With 2017-10 the list of values have been expanded to four by adding `SetForwardMinStay` and `SetForwardMaxStay`. As all four values *must* be supported the two capabilities have been removed. 

It is now possible to define *supplements* that are only available on given days of the week and supplements that depend on room category.

*Offers* have been improved and extended: besides the discounts, the [xml_element_name]#Offer# element is now also used to transmit the age above which a guest is considered an adult and a number of restrictions (for which 2 new capabilities (starting with `OTA_HotelRatePlanNotif_accept_OfferRule`) have been introduced). Free night discounts have also been slightly updated.

Changes have been made to the algorithm describing the computation of the cost of a stay (see section <<anchor-4.5.2,4.5.2>>): there is now a new step 1b, step 3 has been changed (a rate plan is now applicable even if it has a family offer that doesn't match the stat) and step 4b has been simplified (as we have complete and gapless age brackets for all possible child ages now).

Descriptions have been extended and documented more thoroughly.


==== BaseRates

This action has been introduced with 2017-10.



[[anchor-C.3]]
=== C.3. Minor updates in version 2015-07b

Version 2015-07b is a maintenance release.
The section 4.5 about rate plans has been mostly rewritten with more precise and strict information about how to handle corner cases, especially regarding rebates.

While most of this does not lead to breaking changes _per se_, it is likely that 2015-07 servers that were implemented before the release of 2015-07b would compute costs for stays differently, for lack of a sufficiently strict description of details.

One deliberate breaking change of note is that 2015-07b requests the value of the [xml_attribute_name]#AmountAfterTax# attribute in [xml_element_name]#BaseByGuestAmt# elements to be *&gt; 0*, while 2015-07 used to allow a value *≥ 0*.


[[anchor-C.4]]
=== C.4. Major overhaul in version 2015-07

==== Inventory

In version 2015-07 the Inventory message was changed from `OTA_HotelInvNotif` to `OTA_HotelDescriptiveContentNotif`.
The new message offers the same options as the one used previously beside sending the name of the specific rooms, but allows for much richer descriptions, including pictures.
A high-level mapping between the old Inventory and the new one is as follows:

[options="header"]
|===
| `OTA_HotelInvNotif` | `OTA_HotelDescriptiveContentNotif`
| [xml_element_name]#SellableProduct# | [xml_element_name]#GuestRoom#
| [xml_element_name]#SellableProduct# [xml_attribute_name]#InvTypeCode# | [xml_element_name]#GuestRoom# [xml_attribute_name]#Code#
| [xml_element_name]#SellableProduct# [xml_attribute_name]#InvCode# | [xml_element_name]#TypeRoom# [xml_attribute_name]#RoomID#
| [xml_element_name]#Quantities# [xml_attribute_name]#MaximumAdditionalGuests# | Not needed anymore, see StandardOccupancy
| [xml_element_name]#Occupancy# [xml_attribute_name]#MinOccupancy# | [xml_element_name]#GuestRoom# [xml_attribute_name]#MinOccupancy#
| [xml_element_name]#Occupancy# [xml_attribute_name]#MaxOccupancy# | [xml_element_name]#GuestRoom# [xml_attribute_name]#MaxOccupancy#
| [xml_element_name]#Occupancy# [xml_attribute_name]#AgeQualifyingCode#="8" | [xml_element_name]#GuestRoom# [xml_attribute_name]#MaxChildOccupancy#
| Not previously possible | [xml_element_name]#TypeRoom# [xml_attribute_name]#StandardOccupancy#
| [xml_element_name]#Room# [xml_attribute_name]#RoomClassificationCode# | [xml_element_name]#TypeRoom# [xml_attribute_name]#RoomClassificationCode#
| [xml_element_name]#Amenity# [xml_attribute_name]#AmenityCode# | [xml_element_name]#Amenity# [xml_attribute_name]#RoomAmenityCode#
| [xml_element_name]#Text# | [xml_element_name]#TextItem# > [xml_element_name]#Description#
| Not previously possible | [xml_element_name]#ImageItem# [xml_element_name]#># URL
| [xml_element_name]#Text# (for specific Room) | Not possible anymore
|===


[[anchor-C.5]]
=== C.5. Major overhaul in version 2014-04

Version 2014-04 was a major overhaul.
In most cases, a pre-2014-04 client will not be compatible with a 2014-04 server and viceversa.
Here is a list of major changes in 2014-04.


==== HTTPS layer

The possibility of compression with gzip has been added.

==== FreeRooms

The possibility to send booking restrictions in FreeRooms has been removed as have the corresponding capabilities.
These are better handled by the new RatePlans.

The value of the action parameter has been changed from `FreeRooms` to `OTA_HotelAvailNotif:FreeRooms` for uniformity with the other action values that all follow the `rootElement:actionValue` format.

The possible responses (OTA_HotelAvailNotifRS document) have been re-categorized into four classes: success, advisory (new), warning and error.
For error responses the attributes have changed, fixing a bad OTA interpretation.

Finally, the way deltas and complete transmissions are distinguished has changed.
*All in all FreeRooms are not compatible with any previous version.*


==== GuestRequests

GuestRequests have been heavily refactored. Previous {AlpineBitsR} versions had two type of requests: quotes and booking requests, the current version has three: booking reservations, quote requests and booking cancellations. Also, the client can (and must) now send acknowledgements.


==== SimplePackages

The possible responses (OTA_HotelRatePlanNotifRS document) have been re-categorized into four classes: success, advisory (new), warning and error.
For error responses the attributes have changed, fixing a bad OTA interpretation.


==== Inventory and RatePlans

These are new message types introduced with version 2014-04.

[[anchor-C.6]]
=== C.6. Compatibility between a 2012-05b client and a 2013-04 server

Housekeeping

The client will not send the X-AlpineBits-ClientID field in the HTTP header, since it is not aware of this feature. This will cause authentication problems with those 2013-04 servers that require an ID.

The client will not send the X-AlpineBits-ClientProtocolVersion field in the HTTP header, since it is not aware of this feature. This is no problem: a server that is interested in this, will simply recognize the client as
preceding protocol version 2013-04.

If the client checks the server version it will see 2013-04 - a version it doesn’t recognize.
Likewise, if the client checks the capabilities it might see the `OTA_HotelAvailNotif_accept_deltas` capability.
Client implementers interested to have their 2012-05b client talk to a 2013-04 server should verify this is not a problem for their client software.

FreeRooms

There is no compatibility problem in the request part: the client will not send partial information (deltas), since it is simply not aware of the existence of the feature.
Please be aware that the lack of this feature (obviously) causes more data to be sent to the server, something not all companies that run servers will be happy with.
The server response might contain a [xml_element_name]#Warning# element the client cannot process.
If the client - as it should - carefully parses the response, it will treat this as an error situation and act accordingly.
So basically the client is expected to treat the warnings as error, which might be an issue and this should be tested

GuestRequests

No compatibility problems are expected.

SimplePackages

2013-04 introduced the limitation that all packages sent within a single request must refer to the same hotel.
An older client not aware of this limit might incur an error returned from the server error.

Similar to the FreeRooms case, the server response might contain a [xml_element_name]#Warning# element the client cannot process.
If the client - as it should - carefully parses the response, it will treat this as an error situation and act accordingly.
So basically the client is expected to treat the warnings as an error, which might be an issue and should be tested for.

[[anchor-C.7]]
=== C.7. Compatibility between a 2013-04 client and a 2012-05b server

Housekeeping

The client sends the X-AlpineBits-ClientProtocolVersion field and may send the X-AlpineBits-ClientID field in the HTTP header, but the server will just ignore it - being unaware of the feature.
If the client checks the server version and/or checks the capabilities - as it should - it will note the missing features and not use them.  Hence, technically there is no problem with this combination.

FreeRooms

The client will not send partial information (deltas), since the server does not export the `OTA_HotelAvailNotif_accept_deltas` capability.

The server will never send a response with a [xml_element_name]#Warning# element.
This is not a problem for the client.

GuestRequests

In 2013-04 the form of the [xml_element_name]#ID# attribute is not any more restricted.
It has become a free text field.
An older server might insist on the old form and throw an error.

SimplePackages

The server will never send a response with a [xml_element_name]#Warning# element.
This is not a problem for the client.

