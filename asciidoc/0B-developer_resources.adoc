== B. pass:normal[{AlpineBitsR} developer resources]

The {AlpineBitsR} development home page is at https://www.alpinebits.org/developers/.
There are resources linked from that page that help test one’s implementation.

Public repositories with schema files and example code snippets are available online at https://gitlab.com/alpinebits/. Contributions are welcome (any programming language).

